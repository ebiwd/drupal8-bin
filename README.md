# Drupal Development Tools

This folder contains scripts used to aid local drupal development, it should be installed as a submodule in the bin directory of a drupal project.

## Installation of development scripts

`git submodule add --force https://gitlab.ebi.ac.uk/ebiwd/drupal8-bin.git bin`

## Update of development scripts

`git submodule update --remote --force bin`

## Usage of development scripts

- `bin/dev <options>` - spin up/down local development environment
- `bin/dev drupal <commands>` - Can use drupal console commands
- `bin/dev drush <commands>` - Can use drush commands
- `bin/dev dump <sql|files|files-all|all> <dev|stage|prod> <backup>` - to pull files/db dumps from vms, 3rd parameter optional to get dump from backup
- `bin/deploy <options>` - deploy source code to VMs

See each script for help with options.

## Copying template files for a new project

To start a new drupal8 project, copy all of the files from the template directory into the root of the new repo and remote '.template' from the filenames, eg .editorconfig.template becomes .editorconfig, .env.template becomes .env, etc.

Edit the .env, README.md and composer.json files to suit.

## Typical workflow for updating/testing an existing drupal site

- `git clone --recursive git@gitlab.ebi.ac.uk:ebiwd/<repo>` - clone drupal site to local machine
- `bin/dev quick` is an macro for
  - `bin/dev dump <sql|files|files-all|all> <dev|stage|prod> <backup>` - to get files/db dumps, 3rd parameter optional to get dump from backup
  - `bin/dev up`
  - `bin/dev login`

## Using composer to add/remove drupal modules/themes

- `bin/dev composer require drupal/menu_link_attributes:^1.0`
- `bin/dev composer remove drupal/menu_link_attributes`
- add `composer.json`, `composer.lock` and new code to repo

## Using composer to add module/theme from non-drupal repo

- Add new entry in `repositories` section of composer.json
```
{
  "type": "vcs",
  "no-api": true,
  "url": "https://github.com/ebiwd/drupal_8_ebi_framework"
}
```
- `bin/dev composer require ebiwd/drupal_8_ebi_framework:dev-8.x-1.1`

### Composer memory error solution

If you happen to get memory exhausted errors then please try with below command.

`php -d memory_limit=-1 /usr/local/bin/composer <YOUR COMMDAND>`

e.g `php -d memory_limit=-1 /usr/local/bin/composer require "drupal/views_base_url:^1.0"`

## Custom code

- Any code in `web/(modules|themes|profiles)/custom` or `web/sites/*/(modules|themes|profiles)/custom` is preserved

## Typical workflow for creating a new site

- `rm -rf _*` - to clear existing database and files
- `bin/dev up` - to spin up docker containers for local development
- `bin/dev composer install` - to build web folder from composer.lock
- `bin/dev install` - to run site installer
- `bin/dev launch` - to launch browser
- `bin/dev down` - to spin down docker containers

## Diagnostics

- `bin/dev logs` - tail logs from containers
- `bin/dev mailhog` - launch mailhog to view mail sent my containers
- `bin/dev pma` - launch phpMyAdmin to view database

## Pre-requisites (OSX)

You will need
- [Docker Community Edition](https://www.docker.com/community-edition#/download) insalled on your development machine
- SSH keys for drupal apache users in ~/.ssh, and ~/.ssh/config configured with
```
Host ves-ebi-?? ves-hx-?? ves-pg-?? ves-oy-?? wp-np?-?? wp-p?m-??
  IdentityFile ~/.ssh/%r
  StrictHostKeyChecking no
  BatchMode yes
```
