#!/bin/bash

# halt on errors
set -e;
set -u;

ENVIRONMENT=local;

# determine root of repo
ROOT=$(cd $(dirname ${0})/.. 2>/dev/null && pwd -P);
cd ${ROOT};
# set environment variables
set -a; source ${ROOT}/.env; set +a;

function usage {
  echo "Local development docker utilities";
  echo "";
  echo "Usage: bin/dev <function> [parameters]";
  echo "";
  echo "  Function is one of:";
  echo "    up                 - spin up docker container for local development";
  echo "    down               - spin down docker container for local development";
  echo "";
  echo "    launch [url]       - open browser for local development";
  echo "    login              - open browser and login ";
  echo "    pma|solr|mailhog   - open local development utilities";
  echo "";
  echo "    logs [container]   - view logs from docker container";
  echo "      optional [container] is one (or more) of apache|php|mysql|pma|mailhog|memcached";
  echo "";
  echo "    ssh [command]";
  echo "      optional [command] can be shell command to run, otherwise will be interactive shell";
  echo "";
  echo "    drush <command>";
  echo "      <command> is drush command to run";
  echo "    cc|cr [options]    - clear caches";
  echo "    rr <options>       - install and run registry_rebuild";
  echo "    dl [project]       - installs project in sites/all/modules/dev";
  echo "    updb <options>     - run database updates";
  echo "    cron <options>     - run cron";
  echo "    en|dis [module]    - enables/disables module";
  echo ""
  echo "    install            - perform site install via drush";
  echo "    create-aliases     - create alias file for dev|stage|prod and copy to ~/.drushrc";
  echo "";
  echo "    quick <env>        - clone sql and files from dev|stage|prod, spin up docker,";
  echo "                           initialise solr (if enabled), login in browser";
  echo "";
  exit 1;
}

# check for example content in .env
[ ${PROJECT_NAME} = "example.com" ] && echo "Please edit .env before continuing" && exit 1;

# check system requirements
if ! which docker-compose >/dev/null 2>&1; then
  echo "This script requires docker-compose installed on the local machine";
fi;

# validate the command
[ -z "${1:-}" ] && usage;

# get the command and run it
COMMAND="${1}"; shift;
case "${COMMAND}" in
  create_directories)
    echo "Creating required directories"
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_ALIASES_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_FILES_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_PRIVATE_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_SETTINGS_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_SQLDUMP_SRC}
    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_TEMP_SRC}
    ;;

  create_symlinks)
    echo "Creating symlinks"
    [ -d ${USER_FILES} ] && chmod u+w ${DRUPAL_SITE_PATH} ${USER_FILES} && rm -rf ${USER_FILES} || true
    ln -sf ${USER_FILES_SYMLINK} ${USER_FILES}
    ;;

  destroy_symlinks)
    echo "Destroying symlinks"
    rm -f ${USER_FILES} || true
    ;;

  create_config)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    echo "Creating drupal settings"
    mkdir -p ${LOCAL_CORE_PATH}/${DRUPAL_SITE_PATH}
    mkdir -p ${LOCAL_CORE_PATH}/${DRUPAL_CONF_PATH}
    [ -f ${DRUPAL_SETTINGS_PHP} ] && chmod u+w ${DRUPAL_SETTINGS_PHP} || true
    [ -d ${DRUPAL_SITE_PATH} ] && chmod u+w ${DRUPAL_SITE_PATH} || true
    cp ${DRUPAL_DEFAULT_SETTINGS_PHP} ${DRUPAL_SETTINGS_PHP}
    echo "\$""databases['default']['default'] = array ('database' => '${DOCKER_DATABASE}', 'username' => '${DOCKER_DATABASE_USER}', 'password' => '${DOCKER_DATABASE_PASS}', 'host' => 'mysql', 'port' => '3306', 'driver' => 'mysql', 'prefix' => '', 'collation' => 'utf8mb4_general_ci');" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""settings['hash_salt'] = '12345678901234567890';" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""settings['file_private_path'] = '../${RELATIVE_PRIVATE_SRC}';" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""config['system.file']['path']['temporary'] = '../${RELATIVE_TEMP_SRC}';" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""config_directories = array('config_sync_directory' => '../${DRUPAL_CONF_PATH}');" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""settings['config_sync_directory'] = '../${DRUPAL_CONF_PATH}';" >> ${DRUPAL_SETTINGS_PHP}
    echo "\$""base_url = 'http://${DOCKER_URL}:${DOCKER_PORT}';" >> ${DRUPAL_SETTINGS_PHP}
    echo "Configuring git hooks"
    cp -r ${LOCAL_CORE_PATH}/"bin/git_hooks/" ${LOCAL_CORE_PATH}/".git/hooks/"
    chmod -R +x ${LOCAL_CORE_PATH}/".git/hooks/"
    ;;

  destroy_config)
    echo "Destroying drupal settings"
    [ -f ${DRUPAL_SETTINGS_PHP} ] && chmod u+w ${DRUPAL_SETTINGS_PHP} || true
    [ -d ${DRUPAL_SITE_PATH} ] && chmod u+w ${DRUPAL_SITE_PATH} || true
    rm -f ${DRUPAL_SETTINGS_PHP} || true
    ;;

  quick)
    ${ROOT}/bin/dump sql $*;
    ${ROOT}/bin/dump files $*;
    ${ROOT}/bin/dev up;
    ${ROOT}/bin/dev db-wait;
    ${ROOT}/bin/dev login;
    ;;

  pre_up)
    ${ROOT}/bin/dev create_directories
    ${ROOT}/bin/dev create-vendor
    ${ROOT}/bin/dev create_symlinks
    ;;

  post_up)
    ${ROOT}/bin/dev create_config
    ;;

  post_stop)
    ${ROOT}/bin/dev destroy_config
    ${ROOT}/bin/dev destroy_symlinks
    ;;

  post_prune)
    ${ROOT}/bin/dev post_stop
    rm -rf ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}
    ;;

  # spin up docker containers
  up)
    echo "Starting up containers for for ${PROJECT_NAME}..."
    ${ROOT}/bin/dev pre_up;

    docker-compose pull --parallel;
    docker-compose up -d --remove-orphans;

    ${ROOT}/bin/dev post_up;
    ;;

  # spin down docker containers
  stop | down)
    echo "Stopping containers for ${PROJECT_NAME}...";
    docker-compose stop;

    ${ROOT}/bin/dev post_stop;
    ;;

  prune)
    echo "Removing containers for ${PROJECT_NAME}..."
    docker-compose down -v

    ${ROOT}/bin/dev post_prune
    ;;

  # launch container admin sites
  pma | solr | mailhog)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    ${ROOT}/bin/dev launch http://${COMMAND}.${DOCKER_URL}:${DOCKER_PORT};
    ;;

  # launch site, with one-time login
  login)
    echo "Getting one-time-login url...";
    URL=$(${ROOT}/bin/dev drush user-login '/' | grep -F "${PROJECT_NAME}");
    ${ROOT}/bin/dev launch ${URL%?};
    ;;

  # open browser
  launch)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    if [ -d "/Applications/Google Chrome.app" ]; then
      # force chrome as browser, as this does not need adjustment in /etc/hosts
      BROWSER="-a/Applications/Google Chrome.app";
    else
      BROWSER="";
      # insert domain into /etc/hosts
      if ! grep -Fq "${DOCKER_URL}" /etc/hosts; then
        echo "Adding ${DOCKER_URL} to /etc/hosts";
        sudo $SHELL -c "echo '127.0.0.1 ${DOCKER_URL} # added $(date) by ${ROOT}/bin/dev' >> /etc/hosts";
      fi;
    fi;

    if [ -z "${1:-}" ]; then
      DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
      open "${BROWSER}" http://${DOCKER_URL}:${DOCKER_PORT};
    else
      open "${BROWSER}" ${1};
    fi;
    ;;

  # open ssh-like shell to docker container
  bash | ssh | shell)
    # we need to use docker exec $CONTAINER rather than docker-compose exec php
    # as you cannot use -w and -e with docker-compose exec
    CONTAINER=$(docker ps --filter name="${PROJECT_NAME}_php" --format "{{ .ID }}");
    COLUMNS=$(tput cols);
    LINES=$(tput lines);
    if [ -z "${1:-}" ]; then
      docker exec -w /var/www/drupal/${PROJECT_NAME} -ti -e COLUMNS -e LINES ${CONTAINER} bash;
    else
      docker exec -w /var/www/drupal/${PROJECT_NAME} -ti -e COLUMNS -e LINES ${CONTAINER} bash -c "$*";
    fi;
    ;;

  composer)
    if which -s composer; then
      echo "Using local composer";
      COMPOSER_MEMORY_LIMIT=-1 composer $*;
    else
      ${ROOT}/bin/dev ssh "COMPOSER_MEMORY_LIMIT=-1 composer $*";
    fi;
    ;;

  # run drush command on docker container
  drush)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    ${ROOT}/bin/dev ssh "${RELATIVE_VENDOR_PATH}/bin/drush --root=${VM_CORE_PATH}/${RELATIVE_DOCUMENT_ROOT} --uri=${DOCKER_URL}:${DOCKER_PORT} $*";
    ;;

  drupal)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    ${ROOT}/bin/dev ssh "${RELATIVE_VENDOR_PATH}/drupal/console/bin/drupal --root=${VM_CORE_PATH}/${RELATIVE_DOCUMENT_ROOT} $*";
    ;;

  cc | cr)
    ${ROOT}/bin/dev drush ${COMMAND} "$*";
    if docker-compose ps memcached 2>/dev/null | grep -Fq memcached; then
      docker-compose restart memcached;
    fi;
    ;;

  rr)
    ${ROOT}/bin/dev ssh "[ ! -d /root/.drush/registry_rebuild ] && drush dl registry_rebuild && drush cc drush || true";
    ${ROOT}/bin/dev drush rr "$*";
    ;;

  updb | cron | en | dis)
    ${ROOT}/bin/dev drush ${COMMAND} "$*";
    ;;

  dl)
    ${ROOT}/bin/dev drush ${COMMAND} --destination=sites/all/modules/dev "$*";
    ;;

  # view logs on docker container
  logs)
    docker-compose logs -f $*;
    ;;

  # create .gitignore from example.gitignore (D8)
  create-gitignore)
    # rename example.gitignore to .gitignore
    if [ -f ${LOCAL_CORE_PATH}/example.gitignore ] && [ ! -f ${LOCAL_CORE_PATH}/.gitignore ];
    then
      cp -pv ${LOCAL_CORE_PATH}/example.gitignore ${LOCAL_CORE_PATH}/.gitignore;
    fi;
    ;;

  # build vendor directory from composer.json (D8)
  create-vendor)
    ${ROOT}/bin/dev composer install --no-progress;
    ;;

  # ensure volumes are mounted
  mount-volumes)
    if docker-compose ps php 2>/dev/null | grep -Fq php; then
      docker-compose restart php;
      docker-compose restart memcached;
      ${ROOT}/bin/dev create-settings;
    fi;
    ;;

  # run drush site install
  install)
    ${ROOT}/bin/dev drush site-install --sites-subdir=${DEPLOY_SITE_NAME} --account-name=admin --account-pass=admin standard;
    echo "Database initialised with user=admin, pass=admin";
    ${ROOT}/bin/dev launch-advice;
    ;;

  # create aliases file
  create-aliases)
    if [ -d ${LOCAL_CORE_PATH}/${RELATIVE_ALIASES_SRC} ]; then
      rm -rf ${LOCAL_CORE_PATH}/${RELATIVE_ALIASES_SRC}/*.php;
    fi;

    mkdir -p ${LOCAL_CORE_PATH}/${RELATIVE_ALIASES_SRC};

    LOCAL_ALIASES_FILE=${LOCAL_CORE_PATH}/${RELATIVE_ALIASES_SRC}/${PROJECT_NAME}.aliases.drushrc.php;
    echo "Creating ${LOCAL_ALIASES_FILE}";

    echo "<?php" > ${LOCAL_ALIASES_FILE};
    for ENVIRONMENT in dev stage prod; do
      case "${ENVIRONMENT}" in
        dev) REMOTE_HOST=${DEV_SERVER%% *} ;;
        stage) REMOTE_HOST=${STAGE_SERVER%% *} ;;
        prod) REMOTE_HOST=${PROD_SERVER%% *} ;;
      esac;
      echo "
        \$aliases['$ENVIRONMENT'] = array(
          'uri' => 'http://${ENVIRONMENT}.${DEPLOY_INSTANCE}.${PROJECT_NAME}',
          'root' => '${VM_CORE_PATH}/${RELATIVE_DOCUMENT_ROOT}',
          'remote-host' => '${REMOTE_HOST}',
          'remote-user' => '${SSH_OWNER}',
          'path-aliases' => array(
            '%drush-script' => '${VM_CORE_PATH}/${RELATIVE_VENDOR_PATH}/bin/drush',
            '%files' => '${VM_CORE_PATH}/${RELATIVE_FILES_SRC}',
            '%temp' => '${VM_CORE_PATH}/${RELATIVE_TEMP_SRC}',
          ),
        );" >> ${LOCAL_ALIASES_FILE};
    done;
    mkdir -p ~/.drush;
    cp -v ${LOCAL_ALIASES_FILE} ~/.drush/;

    echo "";
    echo "To use aliases to connect to drupal on VM, use";
    echo "  drush @${PROJECT_NAME}.(dev|stage|prod) <command>";
    ;;

  # internal only function to display information about site
  launch-advice)
    DOCKER_URL=${PROJECT_NAME}.docker.localhost;
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    echo;
    echo "Open site with";
    echo "  bin/dev launch";
    echo "or visit";
    echo "  http://${DOCKER_URL}:${DOCKER_PORT}";
    echo;
    echo "Open and login with";
    echo "  bin/dev login";
    echo;
    ;;

  # internal only function to wait for db import for complete
  db-wait)
    if [ -f ${LOCAL_CORE_PATH}/${RELATIVE_SQLDUMP_SRC}/* ] && [ ! -d ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/${DOCKER_DATABASE} ]; then
      echo "Please wait for database to import...";
      # wait for drupal database to start importing
      echo "Waiting for ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/${DOCKER_DATABASE} to be created";
      while [ ! -d ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/${DOCKER_DATABASE} ]; do sleep 1; done;
      # wait for database to restart
      PID=$(ls ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/*.pid);
      echo "Waiting for ${PID} to end";
      while [ -f ${PID} ]; do echo -n ''; done;
      # wait for reboot
      echo -n "Waiting for new ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/*.pid";
      while [ ! -f ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/*.pid ]; do sleep 1; done;
      PID=$(ls ${LOCAL_CORE_PATH}/${RELATIVE_SQLDATA_SRC}/*.pid);
      echo " - ${PID} started";
    fi;
    ;;

  *)
    usage
    ;;
esac;

echo "";
