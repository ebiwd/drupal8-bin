<?php
/**
 * PHP Script to get Drupal8/9/10 core & modules updates available.
 * Created by : Sandeep Kadam <sandeep@ebi.ac.uk>,Mohammad Shuja Malik
 * Modified by : Mohammad Shuja Malik <mshujamalik@ebi.ac.uk>
 * Run it using drush scr(bin/dev drush scr <filename.php>)
 */

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);


$update_list = [];

try {
  
  // Get list of update available.
  $available = update_get_available(TRUE);
  $projects = update_calculate_project_data($available);
  $i = 0;
  foreach ($projects as $project => $value) {
    
    if(empty($value[$project]['latest_version']) && empty($value['latest_version'])){
      continue;
    }
    $update_list[$project]['name'] = $value['name'];
    $update_list[$project]['project_type'] = $value['project_type'];
    $update_list[$project]['existing_version'] = $value['existing_version'];
    $update_list[$project]['latest_version'] = $value['latest_version'];
    $update_list[$project]['security_update'] = isset($value['security']) && $value['security'] ? 'yes' : 'no';
    $i++;
  }
  echo json_encode($update_list);


} catch (HttpExceptionInterface $e) {
  $response = new Response('', $e->getStatusCode());
  $response->prepare($request)->send();
  exit;
}
